package com.cs316.MediaPlayer;

/**
 * Created by samue on 12/13/2017.
 */

public class Song {

    //variables
    private long id;
    private String title;
    private String artist;

    //-----------------------------------------------------------------------------------
    //functions

    //constructor
    public Song(long songID, String songTitle, String songArtist) {
        id=songID;
        title=songTitle;
        artist=songArtist;
    }

    //getter functions
    public long getID()
        {return id;}
    public String getTitle()
        {return title;}
    public String getArtist()
        {return artist;}




}
